global _main

extern _objc_getClass
extern _objc_msgSend
extern _sel_registerName
extern ___CFStringMakeConstantString

default rel

section .data
  NSUserNotification: db 'NSUserNotification',0
  alloc: db 'alloc',0
  init: db 'init',0
  setValueForKey: db 'setValue:forKey:',0

  NSUserNotificationCenter: db 'NSUserNotificationCenter',0
  defaultNotificationCenter: db 'defaultUserNotificationCenter',0
  deliverNotification: db 'deliverNotification:',0

  title_key: db 'title',0
  title_value: db 'Hello World!',0

section .text
_main:
  push rbp
  mov  rbp, rsp
  sub  rsp, 0x50

  ; # Get and store the following things:
  ; 0x08 - class NSUserNotification
  ; 0x10 - selector alloc
  ; 0x18 - selector init
  ; 0x20 - selector setValue:forKey:
  ; 0x28 - CFSTR title
  ; 0x30 - CFSTR Hello World!
  ; 0x38 - class NSUserNotificationCenter
  ; 0x40 - selector defaultNotificationCenter
  ; 0x48 - selector deliverNotification

  lea rdi, [NSUserNotification]
  call _objc_getClass
  mov qword [rbp - 0x08], rax

  lea rdi, [alloc] 
  call _sel_registerName
  mov qword [rbp - 0x10], rax

  lea rdi, [init] 
  call _sel_registerName
  mov qword [rbp - 0x18], rax

  lea rdi, [setValueForKey] 
  call _sel_registerName
  mov qword [rbp - 0x20], rax

  lea rdi, [title_key]
  call ___CFStringMakeConstantString
  mov qword [rbp - 0x28], rax

  lea rdi, [title_value]
  call ___CFStringMakeConstantString
  mov qword [rbp - 0x30], rax

  lea rdi, [NSUserNotificationCenter]
  call _objc_getClass
  mov qword [rbp - 0x38], rax

  lea rdi, [defaultNotificationCenter] 
  call _sel_registerName
  mov qword [rbp - 0x40], rax

  lea rdi, [deliverNotification] 
  call _sel_registerName
  mov qword [rbp - 0x48], rax

  ; # Now construct the notification
  ; rax = [NSUserNotification alloc]
  mov rdi, qword [rbp - 0x08]
  mov rsi, qword [rbp - 0x10]
  call _objc_msgSend

  ; rax/*(rbp - 0x50) = [rax init]
  mov rdi, rax
  mov rsi, qword [rbp - 0x18]
  call _objc_msgSend
  mov qword [rbp - 0x50], rax

  ; [rax setValue:title forKey:@"title"]
  mov rdi, rax
  mov rsi, qword [rbp - 0x20]
  mov rdx, qword [rbp - 0x30]
  mov rcx, qword [rbp - 0x28]
  call _objc_msgSend
  mov qword [rbp - 0x58], rax

  ; rax = [NSUserNotificationCenter defaultUserNotificationCenter]
  mov rdi, qword [rbp - 0x38]
  mov rsi, qword [rbp - 0x40]
  call _objc_msgSend

  ; [rax deliverNotification:(rbp - 0x50)]
  mov rdi, rax
  mov rsi, qword [rbp - 0x48]
  mov rdx, qword [rbp - 0x50]
  call _objc_msgSend

  mov rax, 0
  add rsp, 0x50
  pop rbp
  ret
