CC = clang -lobjc -framework CoreFoundation -framework Foundation

.SUFFIXES: .o .asm

.asm.o:
	yasm -f macho64 -o $@ $*.asm

all: notify

clean:
	rm -rf notify *.o

.phony: all clean
